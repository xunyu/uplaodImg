// 超时状态码
export const STATIC_TIMEOUT = 502;
// 上传响应失败
export const STATIC_RESPONSE_ERROR = 999;
// 上传文件异常
export const STATIC_FILE_ERROR = 1000;
// 图片压缩异常
export const STATIC_COMPRESS_ERROR = 1001;
// 图片转换异常
export const STATIC_EXCHANGE_ERROR = 1002;